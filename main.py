import time
import pickle
from pathlib import Path
import os
from getpass import getpass

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from webdriver_manager.chrome import ChromeDriverManager


def get_user(data: dict) -> str:
    last_user = data.get('user')
    if last_user is None:
        user = input("Username: ")
        return user
    choice = None
    while choice not in {'y', 'n', ''}:
        choice = input(f'Will you use last session user ({last_user})? [Y/n]').lower()
    if choice == 'n':
        user = input('Username: ')
        return user
    return last_user


def load_last_session(path: Path) -> dict:
    if not path.exists():
        return {}
    data = pickle.load(open(path, "rb"))
    return data


class Config:
    datafile_path = Path("./data.pkl")
    data = load_last_session(datafile_path)
    user = get_user(data)
    jumps = {
        'followers': 100000,
        'following': 100000
    }
    scrolls = {
        'followers': 12,
        'following': 17
    }
    delays = {
        'load': 5,
        'after_login': 3
    }
    sleeps = {
        'followers': 1000,
        'following': 1000
    }


def gen_scroll_script(jump: int, scroll_count: int, sleep: int = 1000) -> str:
    return """
        d = document.querySelector('div.isgrP');
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
    """ + "".join([f"d.scrollTo(0, {i * jump}); await sleep({sleep});" for i in range(1, scroll_count)])


def login_and_save_cookies(driver: webdriver, username: str) -> None:
    if Config.data.get('cookies') is None:
        # password = getpass()
        password = input("Password: ")

        driver.get("https://www.instagram.com/")
        try:
            myElem = WebDriverWait(driver, Config.delays['load']).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="username"]')))
        except TimeoutException:
            raise RuntimeError()

        username_input = driver.find_element_by_css_selector('input[name="username"]')
        username_input.send_keys(username)
        password_input = driver.find_element_by_css_selector('input[name="password"]')
        password_input.send_keys(password)
        submit_button = driver.find_element_by_css_selector("button.sqdOP.L3NKy.y3zKF")
        submit_button.click()
        time.sleep(Config.delays['after_login'])  # TODO: error
        Config.data['cookies'] = driver.get_cookies()


def add_cookies_to_driver(driver: webdriver) -> None:
    for cookie in Config.data['cookies']:
        if "domain" in cookie:
            cookie.pop("domain")
        driver.add_cookie(cookie)


def get_people(url: str, driver: webdriver, link_content: str, jump: int, scroll_count: int, sleep: int) -> set:
    driver.get(url)
    try:
        myElem = WebDriverWait(driver, Config.delays['load']).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, 'ul.k9GMp')))
    except TimeoutException:
        raise RuntimeError()
    links = driver.find_elements_by_css_selector("a.-nal3")
    people_link, count = None, None
    for link in links:
        href: str = link.get_attribute('href')
        if link_content in href:
            people_link = link
            count = int(str(link.text).split(" ")[0])
            break

    if people_link is None or count is None:
        raise RuntimeError()

    people_link.click()
    time.sleep(1)
    driver.execute_script(gen_scroll_script(jump, scroll_count, sleep))
    time.sleep(5)
    links = driver.find_elements_by_css_selector("a.FPmhX.notranslate._0imsa")
    people = set()
    for i in links:
        href = i.get_attribute('href')
        people.add(href.split("/")[-2])
    driver.execute_script("document.querySelector('.RnEpo.Yx5HN button.wpO6b').click()")
    if len(people) != count:
        raise RuntimeError("Need to increase scroll counts")
    return people


def main():
    if Config.user != Config.data.get('user') and Config.datafile_path.exists():
        os.remove(Config.datafile_path)
    Config.data['user'] = Config.user
    driver = webdriver.Chrome(ChromeDriverManager().install())
    try:
        driver.get("https://www.instagram.com/")
        login_and_save_cookies(driver, Config.user)
        add_cookies_to_driver(driver)
        followers = get_people(f"https://www.instagram.com/{Config.user}/", driver, "followers",
                               Config.jumps['followers'], Config.scrolls['followers'], Config.sleeps['followers'])
        following = get_people(f"https://www.instagram.com/{Config.user}/", driver, "following",
                               Config.jumps['following'], Config.scrolls['following'], Config.sleeps['following'])
    finally:
        driver.close()

    print("==========================================================================================")
    print(f"followers: {len(followers)}")
    print(f"following: {len(following)}")

    print("==========================================================================================")
    print("followers - following:")
    for i in followers - following:
        print(f"{i} -- https://www.instagram.com/{i}/")

    print("==========================================================================================")
    print("following - followers:")
    for i in following - followers:
        print(f"{i} -- https://www.instagram.com/{i}/")

    print("==========================================================================================")
    if Config.data.get('last_followers') is not None:
        print("last_followers - followers:")
        for i in Config.data.get('last_followers') - followers:
            print(f"{i} -- https://www.instagram.com/{i}/")
    Config.data['last_followers'] = followers

    print("==========================================================================================")
    if Config.data.get('last_following') is not None:
        print("last_following - following:")
        for i in Config.data.get('last_following') - following:
            print(f"{i} -- https://www.instagram.com/{i}/")
    Config.data['last_following'] = following

    pickle.dump(Config.data, open(Config.datafile_path, "wb"))


if __name__ == '__main__':
    main()
